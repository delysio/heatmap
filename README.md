# Heatmap

I was inspired by Thom's posts on [one of the retrocomputing groups](https://www.facebook.com/groups/retrocomputers/) on the engineering use of computers, where he showed how data analysis could be solved using a heatmap.

I would like to show how the same problem can be solved using an 8-bit Atari.

Here is [a video](https://www.youtube.com/watch?v=lOL7heb0nOs) where I show how to export data from VisiCalc and how to load it into my program which will do the analysis.

# Resources

 - [Retro Microcomputers, Workstations, Servers and Consoles](https://www.facebook.com/groups/retrocomputers/) Facebook group
 - [Atari 8-bit Computers](https://www.facebook.com/groups/atari8bitcomputers/)  Facebook group
 - [MadPascal](http://mads.atari8.info/doc/madpascal.html) - Modern Pascal compiler for 8-bit Atari and others 
 - [MadAssembler](http://mads.atari8.info/) - Modern assembler for 8-bit Atari
 - [atari.area](http://www.atari.org.pl/) - Polish source information on Atari
